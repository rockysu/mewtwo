project(c_solver VERSION 1.0.0)

set(CMAKE_CXX_STANDARD 14)

add_library(
    c_solver STATIC
        VonNeuman_core.h
        signal_functions.h
        noise_functions.h
        memory_mgmt.h
        math_functions.h
        hamiltonian_constructor.h
        dependency_functions.h
        testfile.cpp #added for cheating CMake to get the correct language recognized
)

add_subdirectory(DSP)

include_directories(./DSP/include)

target_link_libraries(c_solver PUBLIC armadillo)
target_link_libraries(c_solver PRIVATE DSP)
target_link_libraries(c_solver PRIVATE omp)

target_include_directories(DSP PUBLIC "{CMAKE_CURRENT_SOURCE_DIR}")

set(CMAKE_CXX_FLAGS -O3)
set(CMAKE_CXX_FLAGS -fopenmp)
set(CMAKE_C_FLAGS -O3)
set(CMAKE_C_FLAGS -fopenmp)

#set(CMAKE_BUILD_TYPE Debug)
#set(CMAKE_CXX_FLAGS -g)
#set(CMAKE_C_FLAGS  -g)