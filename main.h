//
// Created by Rocky Su on 2019/11/5.
//


void setup_simulation_task();
void multiple_qubit_task();
void DD_filter_transform_mat();
void set_single_q_dd_task();
void set_double_q_dd_task();
void wait_for_key ();
