#include "main.h"
#include <iostream>
#include <cmath>

#include "QPulseSequnceManager.h"
#include "c_solver/VonNeuman_core.h"
#include "QuantumMathTools.h"
#include "gnuplot/gnuplot_i.hpp"

#define sim_result_path "/Users/rocky/mewtwo/sim_results/"
#define sim_result_expt_y "expectation_y"

int main() {

//    set_single_q_dd_task();
    set_double_q_dd_task();
//    setup_simulation_task();
    std::cout << "Calculation Finished!" << std::endl;
    return 0;
};

void DD_filter_transform_mat() {
    arma::vec expect_y = arma::randn(50001,1) * 3 - 1.5;
    arma::vec time_vec = arma::linspace(0,1e-9* 50000, 50001);

    arma::uvec valid_val_pos = arma::find(expect_y<1 && expect_y>-1,0, "first");

    arma::vec error_filtered_expect_y = expect_y(valid_val_pos);
    arma::vec error_filtered_expect_time_vec = time_vec(valid_val_pos);

    double cutoff_val = 0.1;
    uint upper_limit_index = error_filtered_expect_y.size() - 1;
    arma::uvec zero_boundary_indexes = arma::find((error_filtered_expect_y < cutoff_val && error_filtered_expect_y > 2 * cutoff_val), 0, "first");
    if (zero_boundary_indexes.size() != 0) {
        upper_limit_index = zero_boundary_indexes(0);
    }
    arma::vec valid_expect_y = error_filtered_expect_y.subvec(0,upper_limit_index);
    arma::vec valid_time_vec = error_filtered_expect_time_vec.subvec(0,upper_limit_index);


    arma::vec coherent_decay = arma::log(valid_expect_y);
    arma::mat result_temp(2,valid_expect_y.size(), arma::fill::zeros);
    result_temp.row(0) = coherent_decay.t();
    result_temp.row(1) = valid_time_vec.t();
//    Gnuplot g("dots");
//    g.plot_xy( arma::conv_to<std::vector<double >>::from(error_filtered_expect_time_vec) ,arma::conv_to<std::vector<double >>::from(error_filtered_expect_y) ,"FilteredExpt");
//    int time_length_step = 10000;
//    double time_step_size = 1e-9;
//    double tau_max = 1e-6;
//    int n = 10;
//
//    double omega_min = qmt::pi/tau_max;
//    double omega_step_size = qmt::pi / (time_step_size * time_length_step);
//
//    arma::vec tau_vec = arma::linspace(tau_max/n,tau_max,n);
////    std::cout << tau_vec << "\n" << std::endl;
//
//    arma::vec time_vec = arma::linspace(0,time_length_step*time_step_size,time_length_step + 1);
//
//    std::vector<arma::vec> filter_function_TD_set;
//
//    //Construct filter function set
//    for (int i = 0; i < n; ++i) {
//        double delay_time = tau_vec.at(i);
//        arma::vec filter_func_tmp = arma::ones(time_length_step + 1);
//
//        int flip_flag = -1;
//        int step_size_for_every_tau = delay_time / time_step_size;
//
//        for (int time_idx = 1; time_idx < floor(time_length_step / step_size_for_every_tau); ++time_idx) {
//            filter_func_tmp(arma::span(time_idx * step_size_for_every_tau, time_length_step)) =
//                    flip_flag * arma::ones(time_length_step + 1 - time_idx * step_size_for_every_tau);
//            flip_flag = -flip_flag;
//        }
//        filter_function_TD_set.push_back(filter_func_tmp);
//    }
//
//    int index = n - 1;
//    arma::cx_vec filter_func_tmp_fd = arma::fft(filter_function_TD_set[index]);
//    arma::vec A_series =  sqrt(2 * qmt::pi) / (tau_vec[index] * tau_vec[index]) * arma::real(conj(filter_func_tmp_fd) % filter_func_tmp_fd);
//
//    std::cout << A_series.subvec(0,100) << "\n" << std::endl;
//
//    //Moving window for A_series to refine out the delta pulse sequences' position and amplitude;
//    int window_size = omega_min/omega_step_size;
//    arma::vec A_peaks = arma::zeros(A_series.size()/window_size);
//    arma::vec A_peak_pos = arma::zeros(A_series.size()/window_size);
//    for (int j = 0; j < floor(A_series.size()/window_size); ++j) {
//        arma::vec window = A_series.subvec(j*window_size,(j+1)*window_size);
//        A_peaks.at(j) = window.max();
//        A_peak_pos.at(j) = window.index_max();
//    }
//    std::cout << "Peaks of A_series are:" << "\n" << std::endl;
//    std::cout << A_peaks << "\n" << std::endl;
//
//    int m = (n + 1) * (n + 1);
//    arma::mat U = arma::zeros(n,n);
//    for (int i = 1; i <= n; ++i) {
//        for (int k = 1; k < floor(m/n); ++k) {
//            if (i*k <= n) {
//                U.at(i-1,i*k-1) = A_peaks.at(k-1);
//            }
//        }
//    }
//    std::cout << "U mat:" << "\n" << std::endl;
//    std::cout << arma::inv(U) << "\n" << std::endl;


//    arma::mat U = arma::zeros(n,n);

//    //Construct U matrix by FFT filter functions, U is an upper triangular matrix.
//    for (int i = 0; i < n; ++i) {
//        for (int j = i; j < n; ++j) {
//
//        }
//    }
//    Gnuplot g1("histograms");
//    g1.plot_x( A_series,"A_k");
//
//    Gnuplot g2("lines");
//    g2.plot_x( filter_function_TD_set[0],"FilterFunc0");
//
//    Gnuplot g3("lines");
//    g3.plot_x( filter_function_TD_set[9],"FilterFunc9");

    wait_for_key();
}

void setup_simulation_task() {
    std::cout << "Start Setting up simulation task" << "\n" << std::endl;

    VonNeumannSolver solver_obj = VonNeumannSolver(2);

    //Define static Hamiltonian
    double f_qubit = 1.0e9;
    arma::cx_mat H0 = qmt::pauli_z() * 0.5;
//    H0 = H0*f_qubit ;
    std::cout << "H0 :" << "\n" << H0 << std::endl;

    //Define microwave Hamiltonian
    arma::cx_mat H_mw = qmt::pauli_x();
//    std::cout << "H_mw :" << "\n" << H_mw << std::endl;

    //Define initial state
    arma::cx_colvec2 psi = arma::cx_colvec2();
//    psi(0) = 1/sqrt(2);
//    psi(1) = 1/sqrt(2);
    psi(0) = 1;
    arma::cx_mat22  rho0 = psi * psi.t();

    //Define pulse property
    double global_rabi_freq = 5.0e6 * qmt::pi;
    Microwave X_pi_by_2 = create_gate(x_pi_2, global_rabi_freq,0);
    Microwave Y_pi = create_gate(y_pi, global_rabi_freq, 0);

    double tau = 5.0e-7;
    Microwave W = create_fid(tau);

    QPulseSequnceManager manager = QPulseSequnceManager();
    manager.add_pulse(X_pi_by_2);
    for (int i = 0; i < 10; ++i) {
        manager.add_pulse(W);
        manager.add_pulse(Y_pi);
        manager.add_pulse(W);
    }
//    manager.();
//    manager.add_pulse(X_pi_by_2);

    for (int i = 0; i < manager.pulse_list.size(); i++) {
        Microwave mw = manager.pulse_list.at(i);
        if (mw.rabi_freq > 1) {
            solver_obj.add_H1_MW_RWA(H_mw, mw.rabi_freq, mw.phase, mw.freq, mw.get_start_time(), mw.get_end_time());
        }
    }

    //Adding Hamiltonians to solver
//    solver_obj.add_H0(H0); //No static Hamiltonian under rotating frame
//    solver_obj.add_static_gauss_noise(H0,2e-6);
    solver_obj.add_1f_noise(H0,2e5,1);

    solver_obj.set_number_of_evalutions(20);

    //Calculation launched
    int num_of_steps = 50000;
    double time_resolution = 1e-9;
    double total_time = num_of_steps * time_resolution;
    arma::vec time_vec = arma::linspace(0,total_time,num_of_steps + 1);

    auto cal_start_time = std::chrono::steady_clock::now();
    solver_obj.calculate_evolution(rho0,0.0, total_time, num_of_steps);
    auto cal_end_time = std::chrono::steady_clock::now();
    std::cout << "Cal time total (us):" << "\n" << std::chrono::duration_cast<std::chrono::microseconds>(cal_end_time - cal_start_time).count()<< std::endl;

    //Obtaining results
    arma::cx_cube result = solver_obj.get_all_density_matrices();
//    std::cout << "Result :" << "\n" << result << std::endl;

    arma::cx_cube operators = arma::cx_cube(2,2,3);
    operators.slice(0) = qmt::pauli_z();
    operators.slice(1) = qmt::pauli_x();
    operators.slice(2) = qmt::pauli_y();

    //Calculate expectation values
    arma::mat expect_vals = solver_obj.return_expectation_values_for_ensemble(operators);
//    std::cout << "Expectation Values :" << "\n" << exspect_vals << std::endl;
    stdvecvec results = mat_to_std_vec(expect_vals);

    arma::mat coherent_decay_functions = arma::log(expect_vals);
    stdvec coherent_decay_y = (mat_to_std_vec(coherent_decay_functions)).at(2);

    //Plotting
    Gnuplot g1("dots");
    g1.plot_x( results[0],"z");

    Gnuplot g2("dots");
    g2.plot_x( results[2],"y");

    Gnuplot g3("dots");
    g3.plot_x( results[1],"x");

    Gnuplot g4("lines");
    g4.plot_xyz(results[1],results[2],results[0],"Bloch Sp");

    Gnuplot g5("dots");
    g4.plot_x(coherent_decay_y,"Decay Func of y");

    wait_for_key();
};

void multiple_qubit_task() {
    std::cout << "Start Setting up multiple qubit simulation task" << "\n" << std::endl;

    int number_of_qubit = 7;
    int state_size = pow(2,number_of_qubit);

    VonNeumannSolver solver_obj = VonNeumannSolver(state_size);

    //Define static Hamiltonian but we are not going to add static Hamiltonian under rotating frame.
    double f_qubit = 1.0e9;
    arma::cx_mat H0 = 0.5 * qmt::pauli_z();

    //Define microwave Hamiltonian
    arma::cx_mat H_mw = qmt::pauli_x();
//    std::cout << "H_mw :" << "\n" << H_mw << std::endl;

    //Define initial state
    arma::cx_colvec psi = arma::cx_colvec(state_size);
//    psi(0) = 1/sqrt(2);
//    psi(1) = 1/sqrt(2);
    psi(1) = 1;
    arma::cx_mat rho0 = psi * psi.t();

    //Define pulse property
    double global_rabi_freq = 5.0e6 * qmt::pi;

    Microwave X;
    X.freq = 0;
    X.rabi_freq = global_rabi_freq;
    X.phase = 0;
    X.set_pulse_width((qmt::pi / X.rabi_freq) / 4);

    //FID
    double tau = 5.0e-7;
    Microwave W;
    W.set_pulse_width(tau);

    Microwave Y;
    Y.freq = 0;
    Y.rabi_freq = global_rabi_freq;
    Y.phase = qmt::pi / 2;
    Y.set_pulse_width((qmt::pi / Y.rabi_freq) / 2);

    QPulseSequnceManager manager = QPulseSequnceManager();
    manager.add_pulse(X);
    for (int i = 0; i < 4; ++i) {
        manager.add_pulse(W);
        manager.add_pulse(Y);
        manager.add_pulse(W);
    }
    manager.add_pulse(X);

    for (int i = 0; i < manager.pulse_list.size(); i++) {
        Microwave mw = manager.pulse_list.at(i);
        if (mw.rabi_freq > 1) {
            solver_obj.add_H1_MW_RWA(H_mw, mw.rabi_freq, mw.phase, mw.freq, mw.get_start_time(), mw.get_end_time());
            std::cout << "MW with rabi_freq:" << mw.rabi_freq
                      << " phase:" << mw.phase
                      << " freq:" << mw.freq
                      << " start:" << mw.get_start_time()
                      << " duration:" << mw.get_pulse_width()
                      << "\n" << std::endl;
        }
    }

    //Adding Hamiltonians to solver
//    solver_obj.add_H0(H0); //No static Hamiltonian under rotating frame
    solver_obj.add_static_gauss_noise(H0,2e-6);

    solver_obj.set_number_of_evalutions(1);

    //Calculation launched
    int num_of_steps = 5000;
    double time_resolution = 1e-9;
    double total_time = num_of_steps * time_resolution;

    manager.set_up_time(0, total_time, num_of_steps);

    auto cal_start_time = std::chrono::steady_clock::now();
    solver_obj.calculate_evolution(rho0,0.0, total_time, num_of_steps);
    auto cal_end_time = std::chrono::steady_clock::now();
    std::cout << "Cal time total (us):" << "\n" << std::chrono::duration_cast<std::chrono::microseconds>(cal_end_time - cal_start_time).count()<< std::endl;



    //Obtaining results
    arma::cx_cube result = solver_obj.get_all_density_matrices();
//    std::cout << "Result :" << "\n" << result << std::endl;

    arma::cx_cube operators = arma::cx_cube(state_size,state_size,3);
    operators.slice(0) = arma::kron(arma::eye(state_size / 2,state_size / 2), qmt::pauli_z());
    operators.slice(1) = arma::kron(arma::eye(state_size / 2,state_size / 2), qmt::pauli_x());
    operators.slice(2) = arma::kron(arma::eye(state_size / 2,state_size / 2), qmt::pauli_y());
//    std::cout << "Operator matrices are :" << "\n" << operators << std::endl;

    //Calculate expectation values
    arma::mat expect_vals = solver_obj.return_expectation_values_for_ensemble(operators);
//    std::cout << "Expectation Values :" << "\n" << expect_vals << std::endl;
    stdvecvec results = mat_to_std_vec(expect_vals);

    //Plotting
    Gnuplot g1("dots");
    g1.plot_x( results[0],"z");

    Gnuplot g2("dots");
    g2.plot_x( results[2],"y");

    Gnuplot g3("dots");
    g3.plot_x( results[1],"x");

    Gnuplot g4("lines");
    g4.plot_xyz(results[1],results[2],results[0],"Bloch Sp");

    wait_for_key();
};

void set_single_q_dd_task() {
    std::cout << "Start Setting up simulation task" << "\n" << std::endl;

    //Define initial state
    arma::cx_colvec2 psi = arma::cx_colvec2();
    psi(0) = 1;
    arma::cx_mat22  rho0 = psi * psi.t();

    //Define pulse property
    double global_rabi_freq = 2.50e6 * qmt::pi;
    Microwave X_pi_by_2 = create_gate(x_pi_2, global_rabi_freq,0);
    Microwave Y_pi = create_gate(y_pi, global_rabi_freq, 0);

    int n = 32;

    DD_sequence_group_manager DD_manager;
    DD_manager.tau_max = 1e-3;
    DD_manager.divider = n;
    DD_manager.num_of_DD_pulses = 20;
    DD_manager.time_step_size = 1e-7;
    DD_manager.gate_for_initialize = X_pi_by_2;
    DD_manager.gate_for_DD = Y_pi;
    DD_manager.generate_equal_num_sequence_group();

    std::vector<QPulseSequnceManager> seq_group = DD_manager.get_sequence_group();
    arma::mat fft_mat = DD_manager.get_transformation_mat();
    arma::vec omega_samples = DD_manager.get_omega_sampled_vec();

    arma::vec linear_decay_coefficient = arma::zeros(n);
    std::vector<arma::vec> expectations_y = std::vector<arma::vec>(n);
    std::vector<arma::vec> coherence_func_y = std::vector<arma::vec>(n);
    std::vector<arma::vec> time_vecs = std::vector<arma::vec>(n);

//    seq_group.at(0).sequnce_info_stream();
    std::cout << "\n" << fft_mat << "\n" << std::endl;

    auto cal_start_time = std::chrono::steady_clock::now();

    #pragma omp parallel for
    for (int group_index = 0; group_index < seq_group.size(); ++group_index) {
        QPulseSequnceManager pulse_manager = seq_group.at(group_index);

        arma::vec time_vec = arma::linspace(0,pulse_manager.end_time,pulse_manager.steps + 1);
        time_vecs.at(group_index) = time_vec;

        VonNeumannSolver solver_obj = VonNeumannSolver(2);
        double f_qubit = 1.0e9;
        arma::cx_mat H0 = qmt::pauli_z() * 0.5;
        arma::cx_mat H_mw = qmt::pauli_x();

        for (int pulse_index = 0; pulse_index < pulse_manager.pulse_list.size(); ++pulse_index) {
            Microwave mw = pulse_manager.pulse_list.at(pulse_index);
            solver_obj.add_H1_MW_RWA(H_mw, mw.rabi_freq, mw.phase, mw.freq, mw.get_start_time(), mw.get_end_time());
        }

        solver_obj.add_1f_noise(H0,2e3,1);

        solver_obj.set_number_of_evalutions(64);

        solver_obj.calculate_evolution(rho0,pulse_manager.start_time, pulse_manager.end_time, pulse_manager.steps);

        //Obtaining results
        arma::cx_cube result = solver_obj.get_all_density_matrices();
        arma::cx_cube operators = arma::cx_cube(2,2,1);
        operators.slice(0) = qmt::pauli_y();
//        arma::mat expect_vals = solver_obj.return_expectation_values(operators);
        arma::mat expect_vals = solver_obj.return_expectation_values_for_ensemble(operators);

        arma::vec expect_y = expect_vals.as_col();
        expectations_y.at(group_index) = expect_y;

        //Storing Raw Data
        std::stringstream seq_info_path;
        seq_info_path << sim_result_path << "seq_info/" << "seq_info_" << group_index << ".txt";
        std::ofstream seq_info_file(seq_info_path.str());
        seq_info_file << seq_group.at(group_index).sequnce_info_stream().str();
        seq_info_file.close();

        std::vector<double> Y_vec = arma::conv_to<std::vector<double >>::from(expectations_y.at(group_index));
        std::vector<double> X_vec = arma::conv_to<std::vector<double >>::from(time_vec);

        std::stringstream path;
        path << sim_result_path << "expectation_y/" << "expectation_y_" << group_index;

        Gnuplot g("dots");
        g.plot_xy(X_vec,Y_vec,"<Y>", path.str());
    }

    auto cal_end_time = std::chrono::steady_clock::now();
    std::cout << "Cal time total (us):" << "\n" << std::chrono::duration_cast<std::chrono::microseconds>(cal_end_time - cal_start_time).count()<< std::endl;

    std::vector<arma::mat> coherent_decay_data = DD_manager.get_coherence_decay_func_from_expectatios(expectations_y,time_vecs);
    arma::vec R_n = arma::vec(n, arma::fill::zeros);
    for (int i = 0; i < coherent_decay_data.size(); ++i) {
        arma::mat coherent_decay_func = coherent_decay_data.at(i);
        arma::vec Y = coherent_decay_func.row(0).t();
        arma::vec X = coherent_decay_func.row(1).t();

        arma::vec coefficient = (arma::polyfit(X,Y,1));
        R_n(i) = - coefficient(0);

        std::vector<double> X_vec = arma::conv_to<std::vector<double >>::from(X);
        std::vector<double> Y_vec = arma::conv_to<std::vector<double >>::from(Y);

        std::stringstream path;
        path << sim_result_path << "coherent_decay_func/" << "coherent_decay_func_" << i;

        Gnuplot g("dots");
        g.plot_xy(X_vec,Y_vec,"ln(<Y>)", path.str());
    }
    R_n.save("/Users/rocky/mewtwo/sim_results/U_inv_norm",arma::csv_ascii);

    arma::mat U_inv_norm = arma::normalise(arma::inv(DD_manager.get_transformation_mat()),2,1);
    U_inv_norm.save("/Users/rocky/mewtwo/sim_results/U_inv_norm",arma::csv_ascii);

    arma::vec S_w = U_inv_norm * R_n;

    std::stringstream path;
    path << sim_result_path << "noise_density";
    Gnuplot g6("lines");
    g6.plot_xy(arma::conv_to<std::vector<double >>::from(omega_samples),arma::conv_to<std::vector<double >>::from(S_w),"Noise Density Spec vs Sample Rate",path.str());

    std::cout << S_w << "\n" << std::endl;
    wait_for_key();
}


void set_double_q_dd_task() {

}

void wait_for_key () {
    std::cout << std::endl << "Press ENTER to continue..." << std::endl;
    std::cin.clear();
    std::cin.ignore(std::cin.rdbuf()->in_avail());
    std::cin.get();
    return;
}