//
// Created by Rocky Su on 2019/11/6.
//

#include <vector>
#include <memory>
#include <any>
#include <armadillo>
#include <iomanip>

enum gate_type{ x_pi,
                x_pi_2,  // pi/2 x gate
                y_pi,
                y_pi_2,
                fid,
                undefined
};

class Microwave {
private:
    double start_time = 0;
    double end_time = 0;
    double pulse_width = 0;

public:
    double rabi_freq = 0;
    double freq = 0;
    double phase = 0;
    gate_type  type = undefined;
    std::string type_name = "undefined";

public:
    //setters & getters of properties
    void set_start_time(double mw_start_time){
        start_time = mw_start_time;
        end_time = start_time + pulse_width;
    };

    double get_start_time() {
        return this->start_time;
    }

    void set_pulse_width(double mw_pulse_width){
        pulse_width = mw_pulse_width;
        end_time = start_time + mw_pulse_width;
    };

    double get_pulse_width() {
        return pulse_width;
    }

    double get_end_time() {
        return end_time;
    }

    Microwave(const Microwave& mw) {
        start_time = mw.start_time;
        pulse_width = mw.pulse_width;
        end_time = mw.end_time;

        rabi_freq = mw.rabi_freq;
        freq = mw.freq;
        phase = mw.phase;
        type = mw.type;
        type_name = mw.type_name;
    }

    Microwave() {
        start_time = 0.0;
        pulse_width = 0.0;
        end_time = 0.0;

        rabi_freq = 1.0;
        freq = 0.0;
        phase = 0.0;

        type = undefined;
        type_name = "undefined";
    }
};

Microwave create_gate(gate_type type, double rabi_freq, double freq);

Microwave create_fid(double tau);

class QPulseSequnceManager {

public:
    double start_time;
    double end_time;
    double time_grain;
    int steps;

public:
    std::vector<Microwave> pulse_list;
    arma::vec time_vec;
    arma::vec filter_func_TD;
    std::map<double, gate_type> pulse_on_time_axis;

public:
    QPulseSequnceManager add_pulse(Microwave microwave_to_add) {
        Microwave new_mw = microwave_to_add;
        if(pulse_list.size() >= 1) {
            Microwave pre_neighbor = pulse_list.at(pulse_list.size() - 1);
            new_mw.set_start_time(pre_neighbor.get_end_time());
            pulse_list.push_back(new_mw);
        } else {
            pulse_list.push_back(new_mw);
        }
        return *this;
    };

    void set_up_time(double start_time, double end_time, int time_steps) {
        this->start_time = start_time;
        this->end_time = end_time;
        this->steps = time_steps;

        time_grain = (end_time - start_time) / time_steps;
        time_vec = arma::linspace(start_time, end_time, time_steps);
    };

    void set_up_time_with_current_pulses() {
        Microwave last_pulse = this->pulse_list.at(pulse_list.size() - 1);
        this->start_time = 0;
        this->end_time = last_pulse.get_end_time();
        this->steps = this->end_time / (this->time_grain ?:1);
    };

    std::stringstream sequnce_info_stream();
};

class DD_sequence_group_manager {
public:
    int divider;
    int time_steps;
    int num_of_DD_pulses;
    double time_step_size;
    double tau_max;
    Microwave gate_for_initialize;
    Microwave gate_for_DD;
    std::string info_path = "/Users/rocky/mewtwo/sim_results/DD_sequence_group_manager_info/";

private:
    std::vector<QPulseSequnceManager> sequence_group;
    arma::mat transformation_mat;
//    arma::vec omega_vec;
    arma::vec omega_sampled_vec;

public:
    bool time_setting_precheck();
    void generate_sequence_group();
    void generate_equal_num_sequence_group();
    std::vector<QPulseSequnceManager> get_sequence_group();
    arma::mat get_transformation_mat();
    arma::vec get_omega_sampled_vec();
    std::vector<arma::mat> get_coherence_decay_func_from_expectatios(std::vector<arma::vec> expect_val_set, std::vector<arma::vec> time_vecs);
};
