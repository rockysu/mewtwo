//
// Created by Rocky Su on 2019/11/6.
//

#include "QPulseSequnceManager.h"
#include "QuantumMathTools.h"
#include "DataRecorder.h"

Microwave create_gate(gate_type type, double rabi_freq, double freq) {
    Microwave new_mw = Microwave();
    new_mw.type = type;
    new_mw.rabi_freq = rabi_freq;
    new_mw.freq = freq;

    switch(type) {
        case x_pi:
            new_mw.set_pulse_width((qmt::pi / rabi_freq)/2);
            new_mw.phase = 0;
            new_mw.type_name = "x_pi";
            break;
        case x_pi_2:
            new_mw.set_pulse_width((qmt::pi / rabi_freq)/4);
            new_mw.phase = 0;
            new_mw.type_name = "x_pi/2";
            break;
        case y_pi:
            new_mw.set_pulse_width((qmt::pi / rabi_freq)/2);
            new_mw.phase = qmt::pi/2;
            new_mw.type_name = "y_pi";
            break;
        case y_pi_2:
            new_mw.set_pulse_width((qmt::pi / rabi_freq)/4);
            new_mw.phase = qmt::pi/2;
            new_mw.type_name = "y_pi/2";
            break;
    }
    return  new_mw;
}

Microwave create_fid(double tau) {
    Microwave new_mw = Microwave();
    new_mw.type = fid;
    new_mw.set_pulse_width(tau);

    new_mw.rabi_freq = 0;
    new_mw.freq = 0;
    new_mw.phase = 0;
    new_mw.type_name = "FID";
    return  new_mw;
}

std::stringstream QPulseSequnceManager::sequnce_info_stream() {
    std::stringstream descStream;

    descStream << "Pulses timing info:" << "\n" << std::endl;
    descStream << "|-Start Time:" << start_time << std::endl;
    descStream << "|-End Time:" << end_time << std::endl;
    descStream << "|-Number of Steps:" << steps << std::endl;
    descStream << "|-Time Resolution:" << time_grain << std::endl;
    descStream << "\n" << std::endl;

    descStream << "Pulses in this sequence" << "\n" << std::endl;
    descStream << "|---Type---|StartTime-|-Duration-|-EndTime--|--Phase---|-RabiFreq-|---Freq---|" << std::endl;

    for (int i = 0; i < pulse_list.size(); ++i) {
        Microwave pulse = pulse_list.at(i);

        descStream << "|";
        descStream << std::setw(10) << pulse.type_name << "|";
        descStream << std::setw(10) << std::scientific << std::setprecision(2) << pulse.get_start_time() << "|";
        descStream << std::setw(10) << std::scientific << std::setprecision(2) << pulse.get_pulse_width() << "|";
        descStream << std::setw(10) << std::scientific << std::setprecision(2) << pulse.get_end_time() << "|";
        descStream << std::setprecision(2) << pulse.phase/3.14 << "pi" << "|";
        descStream << std::setw(10) << std::scientific << std::setprecision(2) << pulse.rabi_freq << "|";
        descStream << std::setw(10) << std::scientific << std::setprecision(2) << pulse.freq << "|";
        descStream << std::endl;
    }
    descStream << "|----------|----------|----------|----------|----------|----------|----------|" << "\n" << std::endl;
    return  descStream;
};

void DD_sequence_group_manager::generate_sequence_group() {
    int time_length_step = time_steps;

    if (tau_max >= time_length_step * time_step_size) {
        std::cout << "Sequence Manager: Invalid maximum delay time! More time steps or decrease time resolution!" << "\n" << std::endl;
    }
    int n = divider;
    arma::vec tau_vec = arma::linspace(tau_max,tau_max/n,n);

    double omega_sampled_min = qmt::pi / tau_max;  //Minimum sampling frequency for noise spectroscopy.

    double omega_step_size = qmt::pi / (time_step_size * time_length_step);


    arma::vec time_vec = arma::linspace(0,time_length_step * time_step_size,time_length_step + 1);

    arma::vec omega_vec = arma::linspace(0, time_length_step * omega_step_size, omega_step_size + 1);


    std::vector<arma::vec> filter_function_TD_set;


    printf("Construct Filter Func \n");
    //Construct filter function set
    for (int i = 0; i < n; ++i) {
        QPulseSequnceManager manager_tmp = QPulseSequnceManager();
        Microwave initPulse = create_gate(x_pi_2,gate_for_DD.rabi_freq,gate_for_DD.freq);
        manager_tmp.add_pulse(initPulse);

        double delay_time = tau_vec.at(i);
        Microwave FID = create_fid(delay_time/2);
        arma::vec filter_func_tmp = arma::ones(time_length_step + 1);

        int flip_flag = -1;
        int step_size_for_every_tau = delay_time / time_step_size;

        int initPulse_end_index = initPulse.get_end_time()/time_step_size;
        for (int time_idx = 1; time_idx < floor(time_length_step / step_size_for_every_tau); ++time_idx) {
            manager_tmp.add_pulse(FID);
            manager_tmp.add_pulse(gate_for_DD);
            manager_tmp.add_pulse(FID);
            filter_func_tmp(arma::span(time_idx * step_size_for_every_tau, time_length_step)) =
                    flip_flag * arma::ones(time_length_step + 1 - time_idx * step_size_for_every_tau);
            flip_flag = -flip_flag;
        }
        sequence_group.push_back(manager_tmp);
        filter_function_TD_set.push_back(filter_func_tmp);
//        printf("Construct Complete \n");
    }

    int index = n - 1; //Smallest frequency provides all the necessary transformation amplitude.
    arma::cx_vec filter_func_tmp_fd = arma::fft(filter_function_TD_set[0]);
    filter_func_tmp_fd = filter_func_tmp_fd/time_steps;
    arma::vec A_series =  sqrt(2 * qmt::pi) / (tau_max * tau_max) * arma::real(conj(filter_func_tmp_fd) % filter_func_tmp_fd);

    DataRecord::save_arma_vec(A_series,"/Users/rocky/mewtwo/sim_results/A_series");

//    printf("Construct A series \n");

//        std::cout << A_series.subvec(0,100) << "\n" << std::endl;

    //Moving window for A_series to refine out the delta pulse sequences' position and amplitude;
    int window_size = ceil(omega_sampled_min / omega_step_size);
    arma::vec A_peaks = arma::zeros(A_series.size()/window_size);
    arma::vec A_peak_pos = arma::zeros(A_series.size()/window_size);
    for (int j = 0; j < floor(A_series.size()/window_size); ++j) {
        arma::vec window = A_series.subvec(j*window_size,(j+1)*window_size);
        A_peaks.at(j) = window.max();
        A_peak_pos.at(j) = j * window_size + window.index_max();
    }

//    DataRecord::save_arma_vec(A_peaks,"/Users/rocky/mewtwo/sim_results/A_peaks");
//    DataRecord::save_arma_vec(A_peak_pos,"/Users/rocky/mewtwo/sim_results/A_pos");

//    std::cout << "Peaks of A are: \n" << A_peaks << std::endl;
//    std::cout << "Peaks positions of A are: \n" << A_peak_pos << std::endl;
//    printf("Delta func extracted \n");

    //Construct transformation matrix
    int m = (n + 1) * (n + 1);
    arma::mat U = arma::zeros(n,n);
//    arma::vec omega_for_spectrum = arma::zeros(n);
    for (int i = 1; i <= n; ++i) {
        for (int k = 1; k < floor(m/n); ++k) {
            if (i*k <= n) {
                U.at(i-1,i * k - 1) = A_peaks.at(k-1);
            }
        }
    }

//    arma::mat U_norm = arma::normalise(U,2,1);

    transformation_mat = U;
    U.save("/Users/rocky/mewtwo/sim_results/U",arma::csv_ascii);

    omega_sampled_vec = A_peak_pos.subvec(0,n-1) * omega_step_size;

    printf("Constructed Transformation Mat \n");
};

void DD_sequence_group_manager::generate_equal_num_sequence_group() {
    int n = divider;
    arma::vec tau_vec = arma::linspace(tau_max,tau_max/n,n);
    double omega_sampled_min = qmt::pi / tau_max;  //Minimum sampling frequency for noise spectroscopy.

//    double omega_step_size = qmt::pi / (time_step_size * time_length_step);
//    arma::vec time_vec = arma::linspace(0,time_length_step * time_step_size,time_length_step + 1);
//    arma::vec omega_vec = arma::linspace(0, time_length_step * omega_step_size, omega_step_size + 1);

    std::vector<arma::vec> filter_function_TD_set;

    printf("Construct Filter Func \n");
    //Construct filter function set
    for (int i = 0; i < n; ++i) {
        QPulseSequnceManager manager_tmp = QPulseSequnceManager();
        manager_tmp.time_grain = time_step_size;
        manager_tmp.add_pulse(gate_for_initialize);

        double delay_time = tau_vec.at(i);
        double total_time_for_DD = gate_for_initialize.get_pulse_width() + (delay_time + gate_for_DD.get_pulse_width()) * num_of_DD_pulses;

        Microwave FID = create_fid(delay_time/2);

        int step_size_for_every_tau = delay_time / time_step_size;

        for (int DD_pulse_index = 0; DD_pulse_index < num_of_DD_pulses; ++DD_pulse_index) {
            manager_tmp.add_pulse(FID);
            manager_tmp.add_pulse(gate_for_DD);
            manager_tmp.add_pulse(FID);
        }

        manager_tmp.set_up_time_with_current_pulses();

        arma::vec filter_func_tmp = arma::ones(manager_tmp.steps + 1);
        int end_idx = filter_func_tmp.size() - 1;
        int flip_flag = -1;
        for (int pulse_index = 0; pulse_index < manager_tmp.pulse_list.size(); ++pulse_index) {
            Microwave pulse = manager_tmp.pulse_list.at(pulse_index);
            if (pulse.type_name == gate_for_DD.type_name) {
                int time_idx = pulse.get_end_time() / time_step_size;
                filter_func_tmp(arma::span(time_idx, end_idx)) =
                        flip_flag * arma::ones(end_idx + 1 - time_idx);
                flip_flag = -flip_flag;
            }
        }

        sequence_group.push_back(manager_tmp);
        filter_function_TD_set.push_back(filter_func_tmp);
    }

    arma::cx_vec filter_func_tmp_fd = arma::fft(filter_function_TD_set[0]);
    filter_func_tmp_fd = filter_func_tmp_fd/time_steps;
    arma::vec A_series =  sqrt(2 * qmt::pi) / (tau_max * tau_max) * arma::real(conj(filter_func_tmp_fd) % filter_func_tmp_fd);
    A_series.save(info_path + "A_series",arma::csv_ascii);


    //Moving window for A_series to refine out the delta pulse sequences' position and amplitude;
    double omega_step_size = qmt::pi / (sequence_group[0].end_time);
    int window_size = ceil(omega_sampled_min / omega_step_size);
    arma::vec A_peaks = arma::zeros(A_series.size()/window_size);
    arma::vec A_peak_pos = arma::zeros(A_series.size()/window_size);
    for (int j = 0; j < floor(A_series.size()/window_size); ++j) {
        arma::vec window = A_series.subvec(j*window_size,(j+1)*window_size);
        A_peaks.at(j) = window.max();
        A_peak_pos.at(j) = j * window_size + window.index_max();
    }
    arma::mat A_peak_point = arma::mat(A_peak_pos.size(), 2, arma::fill::zeros);
    A_peak_point.col(0) = A_peak_pos;
    A_peak_point.col(1) = A_peaks;
    A_peak_point.save(info_path + "A_peak_points",arma::csv_ascii);


    //Construct transformation matrix
    int m = (n + 1) * (n + 1);
    arma::mat U = arma::zeros(n,n);
    for (int i = 1; i <= n; ++i) {
        for (int k = 1; k < floor(m/n); ++k) {
            if (i*k <= n) {
                U.at(i-1,i * k - 1) = A_peaks.at(k-1);
            }
        }
    }
    transformation_mat = U;
    U.save(info_path + "U",arma::csv_ascii);

    omega_sampled_vec = A_peak_pos.subvec(0,n-1) * omega_step_size;
    omega_sampled_vec.save(info_path + "omega_sampled_vec",arma::csv_ascii);

    printf("Constructed Transformation Mat \n");
};

bool DD_sequence_group_manager::time_setting_precheck() {
    if ( gate_for_initialize.get_pulse_width() <= time_step_size || gate_for_DD.get_pulse_width() <= time_step_size) {
        std::cout << "DDSequence Error: Pulse width can not be smaller than time step size!" << std::endl;
        return false;
    }

    if (gate_for_DD.get_pulse_width() > tau_max/divider) {
        std::cout << "DDSequence Error: Pulse widths are larger then min paulse delay time!" << std::endl;
        return false;
    }

    return true;
}

std::vector<arma::mat> DD_sequence_group_manager::get_coherence_decay_func_from_expectatios(std::vector<arma::vec> expect_val_set, std::vector<arma::vec> time_vecs){
    std::vector<arma::mat> result_mat;
    for (int i = 0; i < expect_val_set.size(); ++i) {
        arma::vec expect_y = expect_val_set.at(i);
        arma::vec time_vec = time_vecs.at(i);

        //Filter out ill data
        arma::uvec valid_val_pos = arma::find(expect_y<1 && expect_y>-1 && abs(expect_y) >= 1e-5,0, "first");
        arma::vec error_filtered_expect_y = expect_y(valid_val_pos);
        arma::vec error_filtered_expect_time_vec = time_vec(valid_val_pos);

        //Filter out negative data
        double cutoff_val = 1/2.718; //1/e cutoff
        uint upper_limit_index = error_filtered_expect_y.size() - 1;
        arma::uvec zero_boundary_indexes = arma::find((error_filtered_expect_y > cutoff_val && error_filtered_expect_y < cutoff_val + 0.05), 0, "first");
//        std::cout << "expect_y\n" << error_filtered_expect_y << "\n" << std::endl;
//        std::cout << "zero bound index" << zero_boundary_indexes << "\n" << std::endl;
        if (zero_boundary_indexes.size() != 0) {
            upper_limit_index = zero_boundary_indexes(0);
        }
//        std::cout << "upperlimit bound: " << upper_limit_index << "\n" << std::endl;

        arma::vec valid_expect_y = error_filtered_expect_y.subvec(0,upper_limit_index);
        arma::vec valid_time_vec = error_filtered_expect_time_vec.subvec(0,upper_limit_index);

        //Get ln(<Y>) and store to result
        arma::vec coherent_decay = arma::log(valid_expect_y);
        arma::mat result_temp(2,valid_expect_y.size(), arma::fill::zeros);
        result_temp.row(0) = coherent_decay.t();
        result_temp.row(1) = valid_time_vec.t();
        result_mat.push_back(result_temp);
    }
    return result_mat;
};

std::vector<QPulseSequnceManager> DD_sequence_group_manager:: get_sequence_group() {
    if (sequence_group.size() == 0) {
        printf("DD Sequence Manager: No sequence generated yet!");
    }
    return sequence_group;
};

arma::mat DD_sequence_group_manager:: get_transformation_mat() {
    return transformation_mat;
};

arma::vec DD_sequence_group_manager:: get_omega_sampled_vec(){
    return omega_sampled_vec;
};