//
// Created by Rocky Su on 2019/11/22.
//

#include <iostream>
#include <armadillo>
#include <unistd.h>

namespace DataRecord {
    std::string default_path = "./sim_result/";

    void save_arma_vec(arma::vec vector, std::string path = default_path) {
        std::ofstream tmp(path);

        std::vector<double > std_vec = arma::conv_to<std::vector<double >>::from(vector);

        for (unsigned int i = 0; i < vector.size(); i++)
            tmp << std_vec[i] << std::endl;
        tmp.flush();
        tmp.close();
    };

    void save_arma_mat(arma::mat mat, std::string path = default_path) {
        std::ofstream tmp(path);



//        std::vector<double > std_vec = arma::conv_to<std::vector<double >>::from(mat);
//
//        for (unsigned int i = 0; i < vector.size(); i++)
//            tmp << std_vec[i] << std::endl;
        tmp.flush();
        tmp.close();
    };

};
