//
// Created by Rocky Su on 2019/11/11.
//

#include <vector>
#include "c_solver/VonNeuman_core.h"

namespace qsim {

    class task {
        uint iterations = 1;
        uint num_steps = 1;
        uint step_size = 1e-3;

        std::vector<VonNeumannSolver> solvers;
        std::vector<arma::cube> density_mat_results;
        std::vector<arma::cube> measurement_operator;
        std::vector<arma::cube> measure_result;

    public:
        task();

        virtual void hamiltonian_define();
        virtual void sim_parameter_define();
        virtual void measument();
        virtual void plotting();
        virtual void record();

        void perform_task(){
            hamiltonian_define();
            sim_parameter_define();
            measument();
            plotting();
            record();
        }
    };
};