//
// Created by Rocky Su on 2019/11/11.
//
#include <armadillo>
#include <cmath>

namespace qmt{

    const double pi = 3.14159265359;       //pi
    const double h = 6.626e-34;            //Planck constant
    const double h_bar = 6.626e-34/2/pi;   //Reduced Planck constant
    const double e = 1.6e-19;              //Elementary charge
    const double g = 2.01;                 //Electron g-factor
    const double mu_B = 9.27e-24;          //Bohr Magneton
    const double k = 1.38e-23;             //Boltzmann constant

    arma::cx_mat22 pauli_x();

    arma::cx_mat22 pauli_y();

    arma::cx_mat22 pauli_z();
};